package com.creditsuisse.bytecodeomega.session

import org.springframework.data.mongodb.repository.MongoRepository


interface SessionRepository : MongoRepository<Session, String>
