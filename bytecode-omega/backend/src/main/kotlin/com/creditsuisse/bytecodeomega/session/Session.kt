package com.creditsuisse.bytecodeomega.session

import org.springframework.data.annotation.Id

data class Session(
    @Id var id: String?,
    val sessionName: String,
    val sessionType: String,
    val sessionDetails: SessionDetails
)

data class SessionDetails(
    val slides: Collection<Slide>
)

data class Slide(
    val question: String,
    val time: Int,
    val slideNumber: Int,
    val answers: Collection<Answer>
)

data class Answer(
    val answerNumber: Int,
    val value: String
)
