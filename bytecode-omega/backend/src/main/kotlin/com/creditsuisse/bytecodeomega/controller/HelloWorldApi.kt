package com.creditsuisse.bytecodeomega.controller

import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/hello")
class HelloWorldApi {
    @GetMapping
    fun helloWorld(): String {
        return "Hello world!"
    }
}
