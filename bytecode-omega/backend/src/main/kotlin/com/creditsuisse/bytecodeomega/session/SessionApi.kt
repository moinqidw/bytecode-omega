package com.creditsuisse.bytecodeomega.session

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import java.util.*

@RestController
@RequestMapping("/api/v1/session")
class SessionApi @Autowired constructor(
    private val sessionRepository: SessionRepository
) {
    @PostMapping(consumes = ["application/json"], produces = ["application/json"])
    fun createSession(
        @RequestBody session: Session
    ): ResponseEntity<CreateSessionResponse> {
        val result = sessionRepository.save(session)
        return ResponseEntity.ok(CreateSessionResponse(result.id!!))
    }

    @GetMapping(produces = ["application/json"])
    fun getAllSessions(): ResponseEntity<AllSessionsResponse> {
        val sessions = sessionRepository.findAll()
        return ResponseEntity.ok(AllSessionsResponse(sessions))
    }

    @GetMapping("/{sessionId}", produces = ["application/json"])
    fun getSession(@PathVariable sessionId: String): ResponseEntity<Session> {
        val session = sessionRepository.findById(sessionId)

        if (session.isPresent) {
            return ResponseEntity.ok(session.get())
        } else {
            throw UnknownSessionException()
        }
    }

    @PutMapping("/{sessionId}", consumes = ["application/json"])
    fun updateSession(
        @PathVariable sessionId: String,
        @RequestBody session: Session
    ): ResponseEntity<Void> {
        session.id = sessionId
        sessionRepository.save(session)
        return ResponseEntity.noContent().build()
    }
}

data class CreateSessionResponse(
    val sessionId: String
)

data class AllSessionsResponse(
    val sessions: Collection<Session>
)

@ResponseStatus(code = HttpStatus.NOT_FOUND, reason = "Unknown session ID")
class UnknownSessionException : RuntimeException()
