package com.creditsuisse.bytecodeomega

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class BytecodeomegaApplication

fun main(args: Array<String>) {
    runApplication<BytecodeomegaApplication>(*args)
}
