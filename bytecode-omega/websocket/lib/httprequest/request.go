package httprequest

import (
	"io/ioutil"
	"net"
	"net/http"
	"time"
)

const (
	timeout      = 12 * time.Second
	tlsTimeout   = 12 * time.Second
	keepAlive    = 240 * time.Second
	maxIdleConns = 4
)

type Client struct {
	transport *http.Transport
}

type HttpClient interface {
	Get(url string, headers map[string]string, timeout time.Duration) (*HttpResponse, error)
}

type HttpResponse struct {
	Response *http.Response
	Body     []byte
}

func NewHttpClient() *Client {
	c := &Client{}

	c.transport = &http.Transport{
		Dial: (&net.Dialer{
			Timeout:   timeout,
			KeepAlive: keepAlive,
		}).Dial,
		TLSHandshakeTimeout: tlsTimeout,
	}

	c.transport.MaxIdleConnsPerHost = maxIdleConns

	return c
}

func (c *Client) prepareGetRequest(url string, headers map[string]string) (*http.Request, error) {
	// prepare request
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return nil, err
	}

	// add headers
	for k := range headers {
		req.Header.Add(k, headers[k])
	}

	return req, nil
}

func (c *Client) makeRequest(client *http.Client, req *http.Request) (*HttpResponse, error) {
	// make request
	response, err := client.Do(req)
	if response != nil {
		defer response.Body.Close()
	}

	if err != nil {
		return &HttpResponse{
			Response: response,
		}, err
	}

	body, err := ioutil.ReadAll(response.Body)
	if err != nil {
		return &HttpResponse{
			Response: response,
		}, err
	}

	return &HttpResponse{
		Response: response,
		Body:     body,
	}, nil
}

func (c *Client) Get(url string, headers map[string]string, timeout time.Duration) (*HttpResponse, error) {
	client := &http.Client{
		Transport: c.transport,
		Timeout:   timeout,
	}

	// prepare request
	req, err := c.prepareGetRequest(url, headers)
	if err != nil {
		return nil, err
	}

	return c.makeRequest(client, req)
}
