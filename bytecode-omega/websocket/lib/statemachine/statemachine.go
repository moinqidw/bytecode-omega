package statemachine

import (
	"encoding/json"
	"errors"
	"fmt"
	"sync"
	"time"

	"gitlab.com/moinqidw/bytecode-omega/bytecode-omega/websocket/lib/httprequest"
	"gitlab.com/moinqidw/bytecode-omega/bytecode-omega/websocket/lib/logger"
	"gitlab.com/moinqidw/bytecode-omega/bytecode-omega/websocket/schema"
)

type GameState int

const (
	NOT_YET_STARTED GameState = iota
	PAUSE                     // each question finished
	GAMING
	FINISHED // entire presentation finished
)

var gstateToString = map[GameState]string{
	NOT_YET_STARTED: "NOT_YET_STARTED",
	PAUSE:           "PAUSE",
	GAMING:          "GAMING",
	FINISHED:        "FINISHED",
}

var gstateToEnum = map[string]GameState{
	"START":  PAUSE,
	"GAMING": GAMING,
}

var ErrUndefinedGameState = errors.New("undefined game state")
var ErrQuestionNotFound = errors.New("question not found")

type State struct {
	currentQuestionNum int
	currentGameState   GameState
	sessionID          string
	httpClient         *httprequest.Client
	questions          *schema.Session
	backendHost        string
	backendPort        int

	answerCount map[string](map[string]int)
	sync.Mutex
}

func GetGameStateString(state GameState) string {
	return gstateToString[state]
}

func GetGameState(stateName string) (GameState, error) {
	state, present := gstateToEnum[stateName]
	if present {
		return state, nil
	}
	return NOT_YET_STARTED, ErrUndefinedGameState
}

func NewSessionState(sessionID string, backendHost string, backendPort int) *State {
	return &State{
		sessionID:   sessionID,
		backendHost: backendHost,
		backendPort: backendPort,
		answerCount: map[string](map[string]int){},
		httpClient:  httprequest.NewHttpClient(),
	}
}

func (s *State) NextQuestion() bool {
	s.Lock()
	defer s.Unlock()
	questions, err := s.GetQuestions()
	if err != nil {
		return false
	}

	if s.currentQuestionNum+1 >= len(questions.Details.Questions) {
		return false
	}

	s.currentQuestionNum++

	return true
}

func (s *State) GetQuestionID() int {
	s.Lock()
	defer s.Unlock()
	return s.currentQuestionNum
}

func (s *State) SetState(state GameState) GameState {
	s.Lock()
	defer s.Unlock()
	s.currentGameState = state
	return s.currentGameState
}

func (s *State) GetQuestions() (*schema.Session, error) {
	if s.questions != nil {
		return s.questions, nil
	}
	response, err := s.httpClient.Get(fmt.Sprintf("http://%v:%v/api/v1/session/%v", s.backendHost, s.backendPort, s.sessionID), nil, 0)
	session := &schema.Session{}
	if err != nil {
		return nil, err
	}

	err = json.Unmarshal(response.Body, session)
	if err != nil {
		return nil, err
	}
	s.questions = session
	// init answer count
	for _, q := range session.Details.Questions {
		s.answerCount[fmt.Sprint(q.Index)] = map[string]int{}
	}

	return session, nil
}

func (s *State) AddAnswerCount(questionID string, answerID string) {
	s.Lock()
	defer s.Unlock()
	s.answerCount[questionID][answerID] += 1
}
func (s *State) GetAnswerCounts(questionID string) map[string]int {
	s.Lock()
	defer s.Unlock()
	return s.answerCount[questionID]
}

func (s *State) ParseAnswerPayload(answers map[string]int) (*schema.ResultUpdate, error) {
	result := &schema.ResultUpdate{
		State: GetGameStateString(s.currentGameState),
	}

	if s.currentGameState == NOT_YET_STARTED || s.currentGameState == FINISHED {
		return result, nil
	}

	question, err := s.GetCurrentQuestion()
	if err != nil {
		return nil, err
	}

	result.QuestionDes = question.Question
	for _, a := range question.Answers {
		result.Results = append(result.Results, &schema.Result{
			AnswerCount: answers[fmt.Sprint(a.Index)],
			AnswerDes:   a.Value,
			AnswerID:    a.Index,
		})
	}
	return result, nil

}

func (s *State) GetCurrentQuestion() (*schema.SessionQuestion, error) {
	questions, err := s.GetQuestions()
	if err != nil {
		return nil, err
	}
	for _, q := range questions.Details.Questions {
		if q.Index != s.GetQuestionID() {
			continue
		}

		return q, nil
	}
	return nil, ErrQuestionNotFound
}

func (s *State) StartGame(broadcast chan bool) {
	currentQuestion, err := s.GetCurrentQuestion()
	if err != nil {
		logger.Warn("not able to start game")
	}
	time.Sleep(time.Duration(currentQuestion.TimeInSecond) * time.Second)
	if s.NextQuestion() {
		s.SetState(PAUSE)
	} else {
		s.SetState(FINISHED)
	}
	broadcast <- true
}
