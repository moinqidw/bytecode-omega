package main

import (
	"fmt"
	"strings"
	"sync"

	"github.com/gin-gonic/gin"
	"github.com/gorilla/websocket"
	"github.com/spf13/viper"
	"gitlab.com/moinqidw/bytecode-omega/bytecode-omega/websocket/lib/logger"
	"gitlab.com/moinqidw/bytecode-omega/bytecode-omega/websocket/lib/statemachine"
)

type server struct {
	hub *Hub

	// TODO: make me better
	sessionsState map[string]*statemachine.State
	stateLock     sync.Mutex
}

func main() {
	viper.SetDefault("PORT", 8082)

	viper.SetDefault("BACKEND_HOSTNAME", "localhost")
	viper.SetDefault("BACKEND_PORT", 8080)

	viper.AutomaticEnv()

	port := viper.GetInt("PORT")

	hostAndPort := fmt.Sprintf(":%v", port)
	logger.Info("host and port", hostAndPort, "has been successfully loaded")

	srv := &server{
		hub:           newHub(),
		sessionsState: map[string]*statemachine.State{},
	}
	go srv.hub.run()
	r := gin.Default()
	r.GET("/ws/v1/session/:session_id", srv.wsHandler)
	r.GET("/ws/v1/admin/session/:session_id", srv.wsHandler)
	logger.Info("server set up and listening to", hostAndPort)
	r.Run(hostAndPort)
}

func (s *server) wsHandler(c *gin.Context) {
	conn, err := upgrader.Upgrade(c.Writer, c.Request, nil)
	if err != nil {
		logger.Warn(err)
		return
	}

	sessionID := c.Param("session_id")
	isAdmin := strings.Index(c.Request.URL.Path, "/ws/v1/admin/session") > -1
	client := GetNewWsClient(s.hub, conn, s.getSessionState(sessionID), sessionID, isAdmin)
	s.hub.register <- client

	// Allow collection of memory referenced by the caller by doing all work in
	// new goroutines.
	go client.writePump()
	go client.readPump()
}

func GetNewWsClient(hub *Hub, conn *websocket.Conn, sessionState *statemachine.State, sessionID string, isAdmin bool) *Client {

	return &Client{
		hub:          hub,
		conn:         conn,
		sessionID:    sessionID,
		sessionState: sessionState,
		isAdmin:      isAdmin,
		send:         make(chan bool, 256)}
}

func (s *server) getSessionState(sessionID string) *statemachine.State {
	s.stateLock.Lock()
	defer s.stateLock.Unlock()
	sessionState, ok := s.sessionsState[sessionID]
	if ok {
		return sessionState
	}
	sessionState = statemachine.NewSessionState(sessionID, viper.GetString("BACKEND_HOSTNAME"), viper.GetInt("BACKEND_PORT"))
	s.sessionsState[sessionID] = sessionState
	return sessionState
}
