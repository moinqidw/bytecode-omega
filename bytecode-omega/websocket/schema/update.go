package schema

// {"State": "FINISHED"}
// {"State": "PAUSE", "question_description": "abc?", "results": [{"answer_id": 0, "answer_description": "a", "AnswerCount": 1}, {"answer_id": 1, "answer_description": "b", "AnswerCount": 1}, {"answer_id": 2, "answer_description": "c", "AnswerCount": 0}]}

type ResultUpdate struct {
	State       string    `json:"state"` // enum FINISHED, PAUSE (which is START), NOT_YET_STARTED, GAMING
	QuestionDes string    `json:"question_description"`
	Results     []*Result `json:"results"`
}

type Result struct {
	AnswerID    int    `json:"answer_id"`
	AnswerDes   string `json:"answer_description"`
	AnswerCount int    `json:"answer_count"`
}
