package schema

type Session struct {
	Name    string        `json:"sessionName"`
	Details SessionDetail `json:"sessionDetails"`
}

type SessionDetail struct {
	Questions []*SessionQuestion `json:"slides"`
}

type SessionQuestion struct {
	Question     string           `json:"question"`
	Index        int              `json:"slideNumber"`
	TimeInSecond int              `json:"time"`
	Answers      []*SessionAnswer `json:"answers"`
}

type SessionAnswer struct {
	Index int    `json:"answerNumber"`
	Value string `json:"value"`
}
