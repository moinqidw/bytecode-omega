package schema

// General user
// e.g.
// {"selection": 1}
type Answer struct {
	Selection  int    `json:"selection"`
	PlayerName string `json:"player_name"`
}

// Admin
// e.g.
// {"state": "GAMING"}
type ChangeState struct {
	State string `json:"state"` // enum START, GAMING
}
