package main

import (
	"encoding/json"
	"fmt"
	"net/http"
	"time"

	"github.com/gorilla/websocket"
	"gitlab.com/moinqidw/bytecode-omega/bytecode-omega/websocket/lib/logger"
	"gitlab.com/moinqidw/bytecode-omega/bytecode-omega/websocket/lib/statemachine"
	"gitlab.com/moinqidw/bytecode-omega/bytecode-omega/websocket/schema"
)

const (
	writeWait       = 10 * time.Second
	pongWait        = 60 * time.Second
	pingPeriod      = (pongWait * 9) / 10
	maxMessageSize  = 512
	updateRateLimit = 250 * time.Millisecond
)

var (
	newline = []byte{'\n'}
	space   = []byte{' '}
)

var upgrader = websocket.Upgrader{
	ReadBufferSize:  1024,
	WriteBufferSize: 1024,
	CheckOrigin:     func(r *http.Request) bool { return true }, // TODO: remove me
}

// Client is a middleman between the websocket connection and the hub.
type Client struct {
	hub *Hub

	// The websocket connection.
	conn *websocket.Conn

	// stores state of a session
	sessionState *statemachine.State
	// current session ID
	sessionID string

	isAdmin bool
	// Buffered channel of outbound messages
	send chan bool
}

func (c *Client) readPump() {
	defer func() {
		c.hub.unregister <- c
		c.conn.Close()
	}()
	c.conn.SetReadLimit(maxMessageSize)
	c.conn.SetReadDeadline(time.Now().Add(pongWait))
	c.conn.SetPongHandler(func(string) error { c.conn.SetReadDeadline(time.Now().Add(pongWait)); return nil })
	for {
		_, message, err := c.conn.ReadMessage()
		if err != nil {
			if websocket.IsUnexpectedCloseError(err, websocket.CloseGoingAway, websocket.CloseAbnormalClosure) {
				logger.Warn("error: %v", err)
			}
			break
		}

		if c.isAdmin {
			// state change
			answer := &schema.ChangeState{}
			err = json.Unmarshal(message, answer)
			if err != nil {
				logger.Warn("json unmarshal", err)
				break
			}
			state, err := statemachine.GetGameState(answer.State)
			if err != nil {
				logger.Warn("GetGameState", err, "invalue input", answer.State)
				break
			}
			c.sessionState.SetState(state)
			if state == statemachine.GAMING {
				go c.sessionState.StartGame(c.hub.broadcast)
			}
		} else {
			// answer update
			answer := &schema.Answer{}
			err = json.Unmarshal(message, answer)
			if err != nil {
				logger.Warn("json unmarshal", err)
				break
			}

			c.sessionState.AddAnswerCount(fmt.Sprint(c.sessionState.GetQuestionID()), fmt.Sprint(answer.Selection))
		}

		c.hub.broadcast <- true // broadcast if successfully updated
	}
}

func (c *Client) writePump() {
	ticker := time.NewTicker(pingPeriod)
	defer func() {
		ticker.Stop()
		c.conn.Close()
	}()
	// initial update
	answers := c.sessionState.GetAnswerCounts(fmt.Sprint(c.sessionState.GetQuestionID()))

	result, err := c.sessionState.ParseAnswerPayload(answers)
	if err != nil {
		logger.Warn("error: %v", err)
		return
	}
	payload, err := json.Marshal(result)
	if err != nil {
		logger.Warn("error: %v", err)
		return
	}

	err = c.pushUpdate(payload)
	if err != nil {
		logger.Warn("error: %v", err)
		return
	}
	for {
		select {
		case _, ok := <-c.send:
			timeNow := time.Now()
			if !ok {
				c.conn.SetWriteDeadline(timeNow.Add(writeWait))
				// The hub closed the channel.
				c.conn.WriteMessage(websocket.CloseMessage, []byte{})
				return
			}

			n := len(c.send)
			for i := 0; i < n; i++ {
				// discard frequent update msgs
				<-c.send
			}

			answers := c.sessionState.GetAnswerCounts(fmt.Sprint(c.sessionState.GetQuestionID()))

			result, err := c.sessionState.ParseAnswerPayload(answers)
			if err != nil {
				logger.Warn("error: %v", err)
				return
			}
			payload, err := json.Marshal(result)
			if err != nil {
				logger.Warn("error: %v", err)
				return
			}
			err = c.pushUpdate(payload)
			if err != nil {
				logger.Warn("error: %v", err)
				return
			}

			timeDiff := updateRateLimit - time.Since(timeNow)
			if timeDiff > 0 {
				time.Sleep(timeDiff)
			}

		case <-ticker.C:
			c.conn.SetWriteDeadline(time.Now().Add(writeWait))
			if err := c.conn.WriteMessage(websocket.PingMessage, nil); err != nil {
				return
			}
		}
	}
}

func (c *Client) pushUpdate(msg []byte) error {
	if msg == nil {
		return nil
	}

	c.conn.SetWriteDeadline(time.Now().Add(writeWait))
	w, err := c.conn.NextWriter(websocket.TextMessage)
	if err != nil {
		return err
	}

	w.Write(msg)
	return w.Close()
}
