import React, { Component } from 'react';
import { BrowserRouter as Router, Route } from "react-router-dom";
import CreatePage from './create/create';
import SessionPage from './session/session';
import Game from './session/game';
import './App.css';

class App extends Component {
  render() {
    return (
      <div className="App">
        <Router>
          <div>
            <Route path="/" exact component={CreatePage} />
            <Route path="/session/:id" component={SessionPage} />
            <Route path="/admin/session/:id" render={props => <SessionPage {...props} isAdmin />} />
            <Route path="/game" component={Game} />
          </div>
        </Router>
      </div>
    );
  }
}

export default App;
