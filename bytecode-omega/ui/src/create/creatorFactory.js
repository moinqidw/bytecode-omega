import React, { Component } from 'react';
import { Row, Col, Form, FormGroup } from 'reactstrap';
import PropTypes from 'prop-types';
import PresentationCreator from './presentationCreator';
import {SessionTypes} from '../utils/appConstants';


export default class CreatorFactory extends Component {
    constructor(props) {
        super(props);

        this.state = {};

        this.getSessionCreatorByType = this.getSessionCreatorByType.bind(this);
    }

    getSessionCreatorByType(sessionDetails) {
        if (sessionDetails.sessionType === SessionTypes.presentation) {
            return <PresentationCreator sessionDetails={sessionDetails} />
        }
    }
    
    render() {
        if (!this.props.sessionDetails) {
            return <div/>;
        }

        return  <div>
                    {this.getSessionCreatorByType(this.props.sessionDetails)}
                </div>
    }
}

CreatorFactory.propTypes = {
    sessionDetails: PropTypes.shape({
        sessionType: PropTypes.string,
        sessionName: PropTypes.string
    })
}