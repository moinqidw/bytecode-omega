import React, { Component } from 'react';
import { Route, Redirect } from 'react-router';
import { Label, Input, Form, FormGroup, Button, InputGroup, InputGroupAddon } from 'reactstrap';
import _ from 'lodash';
import PropTypes from 'prop-types';
import {createSession} from '../services/sessionService';

export default class PresentationCreator extends Component {
    constructor(props) {
        super(props);

        this.state = { slides: [], currentSlide: 0, successCreatedSession: undefined };
    
        this.onNumberOfSlidesChange = this.onNumberOfSlidesChange.bind(this);
        this.onTimePerSlideChange = this.onTimePerSlideChange.bind(this);
        this.initializeSlides = this.initializeSlides.bind(this);
        this.onQuestionChange = this.onQuestionChange.bind(this);
        this.onAnswerChange = this.onAnswerChange.bind(this);
        this.createSession = this.createSession.bind(this);
        this.addAnswer = this.addAnswer.bind(this);
        this.removeAnswer = this.removeAnswer.bind(this);
        this.nextSlide = this.nextSlide.bind(this);
        this.prevSlide = this.prevSlide.bind(this);
    }

    initializeSlides() {
        if (this.state.numberOfSlides && this.state.timePerSlide) {
            this.setState({ slides: _.times(this.state.numberOfSlides, (idx) => { return { question: undefined,  time: parseInt(this.state.timePerSlide), slideNumber: idx, answers: [] }})});
        }
    }

    onNumberOfSlidesChange(event) {
        this.setState({ numberOfSlides: event.target.value });
    }

    onTimePerSlideChange(event) {
        this.setState({ timePerSlide: event.target.value });
    }

    onQuestionChange(event, slideNumber) {
        event.persist();
        this.setState(state => {
            let slides = _.cloneDeep(state.slides);
            slides[slideNumber].question = _.get(event, 'target.value');
            return { ...state, slides }
        });
    }

    onAnswerChange(event, slideNumber, answerNumber) {
        event.persist();
        this.setState(state => {
            let slides = _.cloneDeep(state.slides);
            slides[slideNumber].answers[answerNumber].value = _.get(event, 'target.value');
            return { ...state, slides }
        });
    }

    addAnswer(slideNumber) {
        let slides = _.cloneDeep(this.state.slides);
        if (!slides[slideNumber].answers) {
            slides[slideNumber].answers = [];
        }
        slides[slideNumber].answers.push({ answerNumber: _.size(slides[slideNumber].answers) });
        
        this.setState(state => {
            return { ...state, slides };
        });
    }

    removeAnswer(slideNumber, answerNumber) {
        let slides = _.cloneDeep(this.state.slides);
        slides[slideNumber].answers = _.filter(slides[slideNumber].answers, function(answer) {
            return answer.answerNumber !== answerNumber
        });

        let i = 0;
        _.forEach(slides[slideNumber].answers, function(answer) {
            answer.answerNumber = i;
            i++;
        });

        this.setState(state => {
            return { ...state, slides };
        });
    }

    nextSlide() {
        this.setState(state => {
            return {...state, currentSlide: state.currentSlide+1 };
        });
    }

    prevSlide() {
        this.setState(state => {
            return {...state, currentSlide: state.currentSlide-1 };
        });
    }

    createSession() {
        const payload = { sessionName: this.props.sessionDetails.sessionName, sessionType: this.props.sessionDetails.sessionType, sessionDetails: { slides: this.state.slides } };
        createSession(payload)
        .then(resp => {
            this.setState({ successCreatedSession: resp });
        })
        .catch(err => { console.log(err) });
    }

    render() {
        if (this.state.successCreatedSession) {
            return <Redirect to={`/admin/session/${this.state.successCreatedSession.sessionId}`}/>
        }

        if (_.isEmpty(this.state.slides)) {
            return <Form>
                <FormGroup>
                    <Label for="numberOfSlides">Number Of Slides</Label>
                    <Input type="number" name="numberOfSlides" id="numberOfSlides" onChange={this.onNumberOfSlidesChange}/>
                </FormGroup>
                <FormGroup>
                    <Label for="timePerSlide">Time Per Slide (In Seconds)</Label>
                    <Input type="number" name="timePerSlide" id="timePerSlide" onChange={this.onTimePerSlideChange}/>
                </FormGroup>
                <Button color="primary" onClick={this.initializeSlides}>Next</Button>
            </Form>
        }

        return <div>
            { this.state.slides.map(slide => {
                return <div key={slide.slideNumber}>
                    { slide.slideNumber === this.state.currentSlide ?
                        <Form>
                            <FormGroup>
                                <Label for={`question${slide.slideNumber}`}>Question {slide.slideNumber + 1}</Label>
                                <Input type="textarea" 
                                        name={`question${slide.slideNumber}`} 
                                        id={`question${slide.slideNumber}`}
                                        value={slide.question}
                                        onChange={(event) => this.onQuestionChange(event, slide.slideNumber)}/>
                            </FormGroup>
                            {
                                slide.answers.map(answer => {
                                    return <FormGroup key={`answer${slide.slideNumber}${answer.answerNumber}`}>
                                        <Label for={`answer${slide.slideNumber}${answer.answerNumber}`}>Answer {answer.answerNumber + 1}</Label>
                                        <InputGroup>
                                            <Input type="text" 
                                                name={`answer${slide.slideNumber}${answer.answerNumber}`} 
                                                id={`answer${slide.slideNumber}${answer.answerNumber}`}
                                                defaultValue={slide.answers[answer.answerNumber].value}
                                                onChange={(event) => this.onAnswerChange(event, slide.slideNumber, answer.answerNumber)}/>
                                            <InputGroupAddon addonType="append"><Button color="danger" onClick={() => this.removeAnswer(slide.slideNumber, answer.answerNumber)}>X</Button></InputGroupAddon>
                                        </InputGroup>
                                    </FormGroup>
                                })
                            }
                            <Button color="success" onClick={() => this.addAnswer(slide.slideNumber)}>Add Answer</Button>
                            <br/><br/>
                            { slide.slideNumber > 0 ? 
                                <Button color="primary" style={{ marginRight: '15px' }}
                                    onClick={this.prevSlide}>Previous</Button>
                            : null }
                            { slide.slideNumber < _.size(this.state.slides)-1 ?
                                <Button color="primary"
                                    disabled={!this.state.slides[slide.slideNumber].question || _.isEmpty(this.state.slides[slide.slideNumber].answers) || _.some(this.state.slides[slide.slideNumber].answers, (answer) => !answer.value)} 
                                    onClick={this.nextSlide}>Next</Button>
                            : null }
                            { slide.slideNumber === _.size(this.state.slides)-1 ?
                                <Button color="success"
                                    disabled={!this.state.slides[slide.slideNumber].question || _.isEmpty(this.state.slides[slide.slideNumber].answers) || _.some(this.state.slides[slide.slideNumber].answers, (answer) => !answer.value)} 
                                    onClick={this.createSession}>Submit</Button>
                            : null }
                        </Form> :
                        null
                    }
                </div>
            })}
        </div>
    }
}