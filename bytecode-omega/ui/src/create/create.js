import React, { Component } from 'react';
import { Redirect } from 'react-router';
import { Container, Row, Col, Button, Form, FormGroup, Label, Input } from 'reactstrap';
import { SessionTypes } from '../utils/appConstants';
import CreatorFactory from './creatorFactory';
import './create.css';
import {getSessions} from '../services/sessionService'


export default class CreatePage extends Component {
    constructor(props) {
        super(props);

        this.state = {
            sessions: [],
            sessionDetails: {},
            previouslyLoadedSession: undefined
        };

        this.onSessionNameChange = this.onSessionNameChange.bind(this);
        this.onSessionTypeChange = this.onSessionTypeChange.bind(this);
        this.showWizard = this.showWizard.bind(this);
        this.showWizardWithDetails = this.showWizardWithDetails.bind(this);
    }

    onSessionNameChange(event) {
        this.setState({ sessionDetails: { ...this.state.sessionDetails, sessionName: event.target.value }});
    }

    onSessionTypeChange(event) {
        this.setState({ sessionDetails: { ...this.state.sessionDetails, sessionType: event.target.value }});
    }

    showWizard() {
        this.setState({ showWizard: true });
    }

    showWizardWithDetails(details) {
        this.setState({ previouslyLoadedSession: details.id });
    }

    componentDidMount() {
        getSessions()
            .then(response => {
                this.setState({ sessions: response.sessions });
            })
            .catch(err => { console.log(err) });
    }

    render() {
        if (this.state.previouslyLoadedSession) {
            return <Redirect to={`/admin/session/${this.state.previouslyLoadedSession}`}/>
        }
        const submissionEnabled = this.state.sessionDetails.sessionName && this.state.sessionDetails.sessionType;
        return  <div>
                    <Container>
                        <div className="main-container">
                        { this.state.showWizard ? <CreatorFactory sessionDetails={this.state.sessionDetails} /> :
                          <div>
                            <Row className="create-new-session-form">
                                <Col xs="12">
                                    <h3>Create new session</h3>
                                    <Form>
                                        <FormGroup>
                                            <Label for="sessionName">Session name</Label>
                                            <Input type="text" name="sessionName" id="sessionName" onChange={this.onSessionNameChange}/>
                                        </FormGroup>
                                        <FormGroup>
                                            <Label for="sessionType">Session Type</Label>
                                            <Input type="select" name="sessionType" id="sessionType" onChange={this.onSessionTypeChange}>
                                                {Object.values(SessionTypes).map(sessionType => <option key={sessionType}>{sessionType}</option>)}
                                            </Input>
                                        </FormGroup>
                                        <Button disabled={!submissionEnabled} color="primary" onClick={this.showWizard}>Create Session</Button>
                                    </Form>
                                </Col>
                            </Row>
                            <br/>
                            {this.state.sessions.length > 0 &&
                                <Row>
                                    <Col>
                                        <h3 className="load-header">Load Prior Session</h3>
                                    </Col>
                                    { this.state.sessions.map(session => <Col xs="12" key={`session${session.sessionName}`}><Button color="link" onClick={() => this.showWizardWithDetails(session)}>{session.sessionName}</Button></Col>) }
                                </Row>
                            }
                          </div>
                        }
                        </div>
                    </Container>
                </div>
    }
}