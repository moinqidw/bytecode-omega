export const ApiUrls = Object.freeze({
    baseUrl: "http://localhost:8080/api/v1",
    createSession: "/session",
    getSessions: "/session"
});