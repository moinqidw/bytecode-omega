import React, { Component } from 'react';
import { Container, Row, Col, Button, Form, FormGroup, Label, Input, ListGroup, ListGroupItem } from 'reactstrap';
import { SessionTypes } from '../utils/appConstants';
import Game from './game';
import _ from 'lodash';

export default class Session extends Component {
    constructor(props) {
        super(props);

        this.state = { sessionDetails: undefined, sessionStatus: 'NOT_YET_STARTED' };

        this.sessionSocket = undefined;

        this.messageHandler = this.messageHandler.bind(this);
        this.startGame = this.startGame.bind(this);
        this.onUsernameChange= this.onUsernameChange.bind(this);
        this.connect = this.connect.bind(this);
        this.startSession = this.startSession.bind(this);
        this.selectAnswer = this.selectAnswer.bind(this);
    }

    messageHandler(event) {
        const data = JSON.parse(event.data);
        if (data.state === "PAUSE") {
            const prevAnswers = _.get(this.state, 'slideDetails.answers');
            const topAnswer = _.get(_.last(_.sortBy(prevAnswers, (ans) => _.get(ans, 'answer_count'))), 'answer_description');
            this.setState({ sessionStatus: 'PAUSE', topAnswer, slideDetails: { question: data.question_description, answers: data.results }});
        } else if (data.state === "GAMING") {
            this.setState({ sessionStatus: 'GAMING', slideDetails: { question: data.question_description, answers: data.results }});
        } else {
            this.setState({ sessionStatus: data.state });
        }
    }

    onUsernameChange(event) {
        this.setState({ username: _.get(event, 'target.value' )});
    }

    connect(isAdmin) {
        const sessionId = _.get(this.props, 'match.params.id');
        if (sessionId) {
            if (!this.sessionSocket) {
                if (!isAdmin) {
                    this.sessionSocket = new WebSocket(`ws://localhost:8082/ws/v1/session/${sessionId}`);
                } else {
                    this.sessionSocket = new WebSocket(`ws://localhost:8082/ws/v1/admin/session/${sessionId}`);
                }
                this.sessionSocket.onmessage = this.messageHandler;
            }
        }
    }

    startGame() {
        this.sessionSocket.send(JSON.stringify({ state: "GAMING" }));
    }

    selectAnswer(answerId) {
        this.sessionSocket.send(JSON.stringify({ selection: answerId }));
    }

    startSession() {
        this.sessionSocket.send(JSON.stringify({ state: "START" }));
    }

    render() {
        const isAdmin = this.props.isAdmin;
        if (isAdmin) {
            this.connect(isAdmin);
        }
        if (!isAdmin && !this.sessionSocket && _.get(this.props, 'match.params.id')) {
            return <div>
                <Form>
                    <FormGroup>
                        <Label for="username">Username</Label>
                        <Input type="text" name="username" id="username" onChange={this.onUsernameChange}/>
                    </FormGroup>
                    <Button onClick={() => this.connect(false)}>Connect</Button>
                </Form>
            </div>
        }

        if (this.state.sessionStatus === "NOT_YET_STARTED") {
            return isAdmin ? 
            <div>
                <h2>Please start the session once you are ready.</h2><br/>
                <Button color="success" onClick={this.startSession}>Start</Button>
                <h1>{`http://[site_url]/session/${_.get(this.props, 'match.params.id')}`}</h1>
            </div> :
            <div>
                <h2>Please wait for the session creator to start the session.</h2>
            </div>
        } else if (this.state.sessionStatus === "PAUSE" || this.state.sessionStatus === "GAMING") {
            return <div>
                <h3>{ _.get(this.state, 'slideDetails.question') }</h3>
                <ListGroup>
                    {_.map(_.get(this.state, 'slideDetails.answers'), answer => <ListGroupItem key={`answer${answer.answer_id}`}>{answer.answer_description}</ListGroupItem>)}
                </ListGroup>
                <br/>
                { this.state.sessionStatus === "GAMING" ? <div>
                    <Game answers={this.state.slideDetails.answers} answerSelected={this.selectAnswer} isAdmin={isAdmin}/>
                </div> : null }
                { isAdmin ?  <Button color="success" onClick={this.startGame}>Start</Button> : null }
                <br/>
                { this.state.topAnswer ? <div><hr/><h2>The winning choice from previous slide: {this.state.topAnswer}</h2></div> : null }
            </div>
        } else if (this.state.sessionStatus === "FINISHED") {
            return <div>
                <h3>The session has been completed.</h3>
                { this.state.topAnswer ? <div><hr/><h2>The winning choice from previous slide: {this.state.topAnswer}</h2></div> : null }
            </div>
        } else {
            return <div>
                <h3>The session was not found. Please double check the session ID.</h3>
            </div>
        }
        return <div/>;
    }
}