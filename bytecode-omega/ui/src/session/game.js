import React, { Component } from 'react';
import { Modal, ModalBody, Button } from 'reactstrap';
import _ from 'lodash';
import Chart from 'chart.js';
import './game.css';

export default class Game extends Component {
    constructor(props) {
        super(props);

        this.selectAnswer = this.selectAnswer.bind(this);
    }

    selectAnswer(event, leg) {
        if (!this.props.isAdmin) {
            const answer = _.find(this.props.answers, answer => answer.answer_description === leg.text);
            this.props.answerSelected(answer.answer_id);
        }
    }

    componentDidMount() {
        this.chart = new Chart("gameChart", {
            type: 'polarArea',
            data: {
                labels: _.map(this.props.answers, 'answer_description'),
                datasets: [{
                    label: '',
                    data: _.map(this.props.answers, () => 0),
                    backgroundColor: _.map(this.props.answers,() =>  `rgba(${Math.random()*255}, ${Math.random()*255}, ${Math.random()*255}, 0.9)`)
                }]
            },
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }]
                },
                legend: {
                    display: true,
                    onClick: this.selectAnswer,
                    labels: { fontSize: 20 }
                }
            }
        });
    }

    shouldComponentUpdate(nextProps, nextState) {
        this.chart.data.datasets[0].data = _.map(this.props.answers, (answer) => answer.answer_count);
        this.chart.update();
        return false;
    }

    render() {
        const height = window.innerHeight;
        const width = window.innerWidth;
        return <Modal isOpen className="main-modal">
            <ModalBody>
                <canvas id="gameChart" width={width} height={height}></canvas>
            </ModalBody>
        </Modal>
    }
} 