export const SessionTypes = Object.freeze({
    select: "Select Session Type",
    presentation: "Presentation"
});