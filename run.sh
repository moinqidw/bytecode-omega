#!/usr/bin/env bash
bytecode-omega/backend/gradlew clean build -p bytecode-omega/backend/ -x test
#docker rmi bytecode-omega_backend -f
docker-compose up --build --remove-orphans -d